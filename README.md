# README #
For most of the Unix and Linux developers, working on command line is a necessity as well as a matter of passion and pride.

Working on command line means remembering at least about 50-100 commands with its various arguments related to file manipulations, text processing, compiling programs and installing packages. We use these commands in a shell, mostly Bash Shell.

Some System Administrators do their entire work using command line interfaces.

At times, remembering the set of commands used in the current directory is difficult. The Bash Shell does provide a history command which keeps a list of all the commands executed. However, searching for a particular command in that list is again a pain.

Intellibash is a simple solution using the concept of ‘locality of reference’ and ‘Pareto principle’.
intellibash

Intellibash is a modified version of the Bash shell. This bash shell now maintains a list of commands for each directory along with the general history. While general history is accessible by the upward arrow, directory specific history is accessible by pressing the downward arrow. The history command has also been modified to show the list the commands used in the current directory along with the global history of commands.

# Install instruction for Intelli Bash

```
sudo apt-get install git build-essential libxml2-dev
git clone https://bitbucket.org/rkvsraman/intelli-bash.git
cd intelli-bash/bash-4.2
./configure
make clean
make
./bash
```