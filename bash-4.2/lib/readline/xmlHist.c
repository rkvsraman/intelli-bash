#include "xmlHist.h"
#define DIRECT "Directory\0"

/*=========================================================================================================*/
/*int main(){
	if(!loadXmlDoc()) return 1 ;
	printf("PREV Hist = %s \n", getPrevHist());
	printf("next Hist = %s \n", getNextHist());
	printf("next Hist = %s \n", getNextHist());
	printf("PREV Hist = %s \n", getPrevHist());
	deletexmlDoc();
	return 0;
}*/

int directory_reload = 0;

void D_Check(){
	if(!hist_doc) return;
	xmlNodePtr root = xmlDocGetRootElement(hist_doc);
	if(root == NULL) return;
	char* path = (char*) malloc(2);
	strcpy (path , "/\0");
	directoryCheck(path,root);

}

void directoryCheck(char* path, xmlNodePtr parent){
	if(!parent) return;
	xmlChar *key;
	xmlNodePtr curr = parent->xmlChildrenNode;
	if(!curr) return ;
	char* temp = (char*) malloc ((strlen(path) +2) * sizeof(char));
	strcpy(temp,path);
	xmlNodePtr next;
	while(curr){
		next = curr->next;
		if ((!xmlStrcmp(curr->name, (const xmlChar *)DIRECT ))){
			struct stat stateinfo;
			key =(xmlChar*) xmlGetProp(curr,"name");
			temp = (char*) realloc(temp, (strlen(temp)+strlen((char*)key) +2));
			strcat(temp,(char*)key);
			if(stat(temp, &stateinfo) == -1){
				xmlDOMWrapRemoveNode(xpathCtx,hist_doc,curr,0);
			}else{
				strcat(temp, "/\0");
				directoryCheck(temp,curr);
			}
			strcpy(temp,path);
			xmlFree(key);
		}
		curr = next;
	}
	free(temp);
}


/*=====================================================================================loadXmlDoc()========*/
int loadXmlDoc(){
#ifdef SMART_HISTORY
	char * h_File = getenv("HOME");
	hist_File = (char *)malloc ((strlen(h_File)+15)*sizeof(char));
	strcpy(hist_File,h_File);
	strcat(hist_File,"/.history.xml");
	hist_doc = xmlParseFile(hist_File);
	if(hist_doc == NULL ) {
		fprintf(stderr,"Document not parsed successfully. \n");
		if(!create_HistFile(hist_File)) return FALSE;
		hist_doc = xmlParseFile(hist_File);
	}	
//	printf("XML LOADED\n");
	xpathCtx = xmlXPathNewContext(hist_doc);
	return TRUE;
#endif
	return FALSE;
}
/*==================================================================================deletexmlDoc()=========*/
void deletexmlDoc(){
#ifdef SMART_HISTORY

	D_Check();
	FILE *fp;
	fp = fopen(hist_File,"w");
	xmlDocDump(fp,hist_doc);
	fclose(fp);
    //if(xpath) free(xpath);
	
	if(xpathCtx)	xmlXPathFreeContext(xpathCtx);
	if(hist_doc)    xmlFreeDoc( hist_doc);
#endif
}

/*===============================================================================findCurrentNode(.)=======*/
xmlNodePtr findCurrentNode ( char* path){

#ifdef SMART_HISTORY
	if(!path || !strlen(path)) return 0;
	int size;
 	xmlNodeSetPtr foundNodes;
	xmlChar *xmlStr = ConvertInput(path,MY_ENCODING);
	xmlXPathObjectPtr xpathObj = xmlXPathEvalExpression(xmlStr, xpathCtx);
	xmlFree(xmlStr);
	xmlNodePtr newChild;
	if(xpathObj) {
		foundNodes = xpathObj->nodesetval;
		size = (foundNodes) ? foundNodes->nodeNr : 0;
		if (size !=0) {
			return foundNodes->nodeTab[size-1];
		}
	}
	
	char *dir = strrchr(path,SEPARATOR);
	if (!dir || (dir==path)) return 0;
	
 	char delims[] = "\"";  int flag =0;
	char *result = NULL;
	result = strtok( dir, delims );
	if(result != NULL){
		result = strtok(NULL,delims);
	}
	path[dir-path] = '\0';	
	newChild = findCurrentNode( path);
	if(!newChild) {
		printf("NO PARENT so Please Make One");
	}	
	return addChild(newChild,DIRECT, result);
#endif
	return 0;
}

/*====================================================================================addChild(...)=========*/
xmlNodePtr addChild( xmlNodePtr root, char* child,char* prop) {

	int rc;
    	xmlChar *tmp;
	if (root == NULL) {
		fprintf(stderr,"empty document\n");
		return 0;
	}
	xmlNodePtr newChild = xmlNewChild(root,NULL,BAD_CAST child, NULL);
	xmlAttrPtr newAttr = xmlNewProp(newChild,(xmlChar *)"name\0",(xmlChar*) prop);
	
	return	newChild;
}
/*====================================================================================isCurrentPath()========*/
int isCurrentPath(){
    char dir[1024], *currentLocation ,newStr[1024];
    currentLocation =  getcwd(dir,1023);
  //  printf("Directory %s\n",dir);
  /*  xpath = (char*)malloc(1024*sizeof(char));
    if(xpath){
        xpath[0]='\0';
    }*/

	strcpy(newStr,"/smartHistory\0");
    strcat(newStr,dir);
        
	if(!xpath || strcmp(xpath,newStr) || directory_reload == 1) {
        

		xpath = newStr;
		xpath[strlen(newStr)] = '\0';
		currentCmdNode = 0;
        directory_reload = 0;
		return FALSE;
	}
 //   free(newStr); 
	return TRUE;
}
/*====================================================================================add_command(.)=========*/
void add_command(char *cmd){   
#ifdef SMART_HISTORY
    char dpath[1024],dir[1000];
    if(strlen(cmd)<=0) return
    isCurrentPath();
    currentCmdNode =0;
    strcpy(dpath,"/smartHistory");
    getcwd(dir,1000);
    strcat(dpath,dir);
    strcat(dpath,"\0");
  //  printf("Path -%s-xxx\n",dpath);
    char * path = convertIntoXpath(dpath);
    xmlNodePtr current  = 	findCurrentNode(path);
    Add_commandToParent(current,cmd);
    free(path);
#endif

}
/*=========================showSmartHistory=============================================*/

void showSmartHistory()
{
        isCurrentPath();
	currentCmdNode =0;
	char * path = convertIntoXpath(xpath);
	xmlNodePtr currentDir  = findCurrentNode(path);
	xmlNodePtr  curr;
	
	curr = currentDir->xmlChildrenNode;
	xmlChar* key;
        printf("\nCommands in this directory\n------------------------------\n");
	while(curr != NULL){
		if ((!xmlStrcmp(curr->name, (const xmlChar *)"command\0"))){
			key =(xmlChar*) xmlGetProp(curr,ConvertInput("name",MY_ENCODING));
			printf("%s\n", (char *)key);
			xmlFree(key);
	    	    }
		    curr = curr->next;
	}
	
}


/*====================================================================================getNextHist()=========*/
char* getNextHist(){
#ifdef SMART_HISTORY

	int flag = 1;

	if(isCurrentPath())
		flag =1;
	else flag =0;
	
	if(!flag || !currentCmdNode){
		char* path = convertIntoXpath(xpath);
		xmlNodePtr curr = findCurrentNode(path);
		free(path);
		if(!curr) return 0;
		currentCmdNode = curr->xmlChildrenNode;
	}else if(currentCmdNode->next){
			currentCmdNode = currentCmdNode->next;
			flag =0 ;
	}

	if(currentCmdNode && !flag) {
		if((!xmlStrcmp(currentCmdNode->name, (const xmlChar *)"command"))){
			xmlChar* key = (xmlChar*)xmlGetProp(currentCmdNode,(xmlChar*) "name");
			return (char*)key;
		}else if(currentCmdNode->prev) currentCmdNode = currentCmdNode->prev;
	}
#endif
	return 0; 
}
/*====================================================================================getPrevHist()=========*/
char* getPrevHist(){
#ifdef SMART_HISTORY
	int flag = 1;
	if(isCurrentPath())
		flag =1;
	else return 0;
	if(currentCmdNode){
			if(!currentCmdNode->prev) return 0;
			currentCmdNode = currentCmdNode->prev;
			xmlChar* key = (xmlChar*)xmlGetProp(currentCmdNode,(xmlChar*) "name");
			return (char*) key;
	}
#endif
	return 0; 
}
	
/*================================================================================Add_command(..)=======*/
int Add_commandToParent(xmlNodePtr parent, char* comd){
	if(!parent) return 0;
	xmlNodePtr xlmChild, curr, firstChild;
	
	curr = parent->xmlChildrenNode;
	xmlChar* key;
	while(curr != NULL){
		if ((!xmlStrcmp(curr->name, (const xmlChar *)"command\0"))){
			key =(xmlChar*) xmlGetProp(curr,ConvertInput("name",MY_ENCODING));
			if(!xmlStrcmp(key,(const xmlChar*)comd)){
				xmlDOMWrapRemoveNode(xpathCtx,hist_doc,curr,0);
				xmlFree(key);
                                
				break;
			}
			xmlFree(key);
	    	    }
		    curr = curr->next;
	}
	
	firstChild = parent->xmlChildrenNode;
	xmlNodePtr child = addChild(parent,"command\0",comd);
	if(firstChild){
		xmlAddPrevSibling(firstChild, child);	
	}
	currentCmdNode = parent->xmlChildrenNode;
        directory_reload = 1;
	return 1;	
}

/*================================================================================create_HistFile(.)=======*/
int create_HistFile(char* uri){
#ifdef SMART_HISTORY

	int rc;
	xmlTextWriterPtr writer;
	xmlChar *tmp;

	writer = xmlNewTextWriterFilename(uri, 0);

	if (writer == NULL) {
		printf("testXmlwriterFilename: Error creating the xml writer\n");
		return FALSE;
	}

	rc = xmlTextWriterStartDocument(writer, NULL, MY_ENCODING, NULL);
	if (rc < 0) {
		printf("testXmlwriterFilename: Error at xmlTextWriterStartDocument\n");
		return FALSE;
	}

	rc = xmlTextWriterStartElement(writer, BAD_CAST DIRECT);
	if (rc < 0) {
		printf("testXmlwriterFilename: Error at xmlTextWriterStartElement\n");
		return FALSE;
	}
	    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "name",BAD_CAST ROOT_NODE);
	    if (rc < 0) {
		return 0;
	     }

	rc = xmlTextWriterEndElement(writer);
	if (rc < 0) {
		printf ("testXmlwriterFilename: Error at xmlTextWriterEndElement\n");
		return FALSE;
	}

	rc = xmlTextWriterEndDocument(writer);
	if (rc < 0) {
		printf("testXmlwriterFilename: Error at xmlTextWriterEndDocument\n");
		return FALSE;
	}
	xmlFreeTextWriter(writer);
	return TRUE;
#endif
}
/*============================================================================convertIntoXpath(.)========*/
char* convertIntoXpath(char* path){
   /* if(path  && (strstr(path,"\"")||strstr(path,"`")||strstr(path,"'")))
    {
       printf("Caught a literal");
        return 0;
    }*/
	if(!path || !strlen(path)) return 0;
   // printf("Path is %s\n",path);
	char* aaaa = (char *) malloc( (strlen(path)+1)*sizeof(char) );
	strcpy(aaaa,path);
	aaaa[strlen(path)] = '\0';

	char delims[] = "/";

  //  printf("AAAA %s\n",aaaa);
    if(!strstr(aaaa,"/")) return 0;
	char* str = strtok(aaaa,delims);
	char* addon = "/Directory[@name=\"\0";
	char *ret = (char*) malloc(sizeof(char)*2 );
	strcpy(ret,"\0\0");
	while(str !=NULL){
       // printf("STR %s\n",str);
		ret = (char*) realloc(ret, sizeof(char) * (strlen(ret) + strlen(str)+strlen(addon)+ 3));
		strcat(ret,addon);
		strcat(ret,str);
		strcat(ret,"\"]\0");
		str = strtok(NULL,delims);
	}
  //  printf("Ret is %s\n",ret);
	free(aaaa);
	return ret;
}

/*=========================================================================================================*/
xmlChar * ConvertInput(const char *in, const char *encoding) {
    xmlChar *out;
    int ret;
    int size;
    int out_size;
    int temp;
    xmlCharEncodingHandlerPtr handler;
    if (in == 0)
        return 0;

    handler = xmlFindCharEncodingHandler(encoding);

    if (!handler) {
        printf("ConvertInput: no encoding handler found for '%s'\n",
               encoding ? encoding : "");
        return 0;
    }
    size = (int) strlen(in) + 1;
    out_size = size * 2 - 1;
    out = (unsigned char *) xmlMalloc((size_t) out_size);
    if (out != 0) {
        temp = size - 1;
        ret = handler->input(out, &out_size, (const xmlChar *) in, &temp);
        if ((ret < 0) || (temp - size + 1)) {
            if (ret < 0) {
                printf("ConvertInput: conversion wasn't successful.\n");
            } else {
                printf
                    ("ConvertInput: conversion wasn't successful. converted: %i octets.\n",
                     temp);
            }
            xmlFree(out);
            out = 0;
        } else {
            out = (unsigned char *) xmlRealloc(out, out_size + 1);
            out[out_size] = 0;  /*null terminating out */
        }
    } else {
        printf("ConvertInput: no mem\n");
    }
    return out;
}
