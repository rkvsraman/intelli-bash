#ifndef _XML_HIST_H
#define _XML_HIST_H

#include <stdio.h>
#include <string.h>
#include <libxml/xmlmemory.h>
#include <libxml/xpathInternals.h>
#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>
#include <sys/stat.h>


#define TRUE 1
#define FALSE 0
#define ROOT_NODE "smartHistory"
#define MY_ENCODING "UTF-8"
#define SEPARATOR '/'

char* hist_File;  

xmlDocPtr hist_doc ;

xmlXPathContextPtr xpathCtx;

xmlNodePtr currentCmdNode;

char* xpath;


int loadXmlDoc();
void deletexmlDoc();
void showSmartHistory();
void D_Check();
void directoryCheck(char* path, xmlNodePtr parent);

xmlNodePtr findCurrentNode (char* path);

xmlNodePtr addChild( xmlNodePtr root, char* child, char* prop);

int isCurrentPath();

void add_command(char *cmd);
int Add_commandToParent(xmlNodePtr parent, char* comd);

int create_HistFile(char* uri);

char* getNextHist();

char* getPrevHist();

char* convertIntoXpath(char* path) ;

xmlChar * ConvertInput(const char *in, const char *encoding) ;
#endif
